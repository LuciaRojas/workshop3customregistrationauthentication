const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user = new Schema({
  usuario: { type: String },
  contrasenna: { type: String },
  nombre: { type: String },
  apellido: { type: String }
  //nombre, apellido, correo electrónico, dirección
});

module.exports = mongoose.model('users', user);